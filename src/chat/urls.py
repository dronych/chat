from django.conf.urls import url

from chat import views

urlpatterns = [
    url(r'^(?P<room_name>[a-z0-9_-]+)/$', views.room, name='room'),
    url(r'^$', views.index, name='index'),
]
